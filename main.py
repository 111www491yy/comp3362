import cv2
import mss
import numpy
import win32gui
import pyautogui
import torchvision
import torch
import numpy as np
import os
from PIL import Image
from torchvision.transforms import transforms as transforms
from time import sleep
import pydirectinput
import keyboard

# Names of app that will be ignored while selecting windows to capture
EXCLUDED_APPS = ["NVIDIA GeForce Overlay DT", "NVIDIA GeForce Overlay",
                 "*Python 3.7.9 Shell*", "Nahimic", "OmApSvcBroker",
                 "Windows Input Experience", "Program Manager", "Settings",
                 "C:\WINDOWS\system32\cmd.exe", "ZPToolBarParentWnd",
                 "Mail", "Inbox - Gmail ‎- Mail"]

transform = transforms.Compose([
    transforms.ToTensor()
])

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def get_all_windows():
    """
    This function gets the names of all running applications

    Return: array that contains the names of all running applications
    """
    windows = []
    counter = 1
    for x in pyautogui.getAllWindows():
        if len(x.title) > 0 and x.title not in EXCLUDED_APPS:
            print(counter, "\t:", x.title)
            windows.append(x.title)
            counter += 1
    return windows


def select_window():
    """
    This function lets the user to select the application to capture

    Return: coordinate of the window of application to capture
            (top-left x coordinate, top-left y coordinate, width, height)
    """
    windows = get_all_windows()

    app_number = int(input("\nNumber of the Application to Capture: "))
    sleep(2)
    app_name = windows[app_number - 1]

    hwnd = win32gui.FindWindow(None, app_name)

    x0, y0, x1, y1 = win32gui.GetWindowRect(hwnd)

    return x0, y0, x1, y1


def capture_screenshot(top, left, width, height):
    """
    This function captures the screenshot of the application selected

    top: top-left y coordinate
    left: top-left x coordinate
    width: width of the window of the application
    height: height of the window of the application

    Return: image
    """
    with mss.mss() as sct:
        monitor = {"top": top, "left": left, "width": width, "height": height}

        img = sct.grab(monitor)
        img = Image.frombytes("RGB", img.size, img.bgra, "raw", "BGRX")
        #img.show()

        return img


def shoot(x, y):
    """
    This function automates the clicking at (x, y) coordinate

    x: x-coordiante to click
    y: y-coordinate to click
    testing: True for testing (Boolean)
    """
    pydirectinput.moveTo(x, y)
    for i in range(3):
        pydirectinput.click()


def get_model(keypoints, path='./keypointsrcnn_5k.pth'):
    model = torchvision.models.detection.keypointrcnn_resnet50_fpn(num_keypoints=keypoints,
                                                                   num_classes=2)
    model.load_state_dict(torch.load(path, map_location=torch.device(device)))
  
    return model


def get_point(outputs):
    num_predict = len(outputs[0]['keypoints'])
    num_keypoints = len(outputs[0]['keypoints'][0])
    headlist = []
    bodylist = []
    try:
        if(num_keypoints == 7):
            for i in range(num_predict):
                # get the detected keypoints
                keypoints = outputs[0]['keypoints'][i].cpu().detach().numpy()
                if outputs[0]['scores'][i] > 0.7:
                    keypoints = keypoints[:, :].reshape(-1, 3)
                    head = (keypoints[0] + keypoints[1] + keypoints[2])/3
                    head = tuple((head[0],head[1]))
                    body = (keypoints[3] + keypoints[4] + keypoints[5]+ keypoints[6])/4
                    body = tuple((body[0],body[1]))
                    headlist.append(head)
                    bodylist.append(body)
        elif(num_keypoints == 17):
            for i in range(num_predict):
                # get the detected keypoints
                keypoints = outputs[0]['keypoints'][i].cpu().detach().numpy()
                if outputs[0]['scores'][i] > 0.7:
                    keypoints = keypoints[:, :].reshape(-1, 3)
                    head = (keypoints[0] + keypoints[1] + keypoints[2])/3
                    head = tuple((head[0],head[1]))
                    body = (keypoints[5] + keypoints[6] + keypoints[11] + keypoints[12])/4
                    body = tuple((body[0],body[1]))
                    headlist.append(head)
                    bodylist.append(body)
    except:
        pass
    return headlist, bodylist


# input image shd be in RGB format
def predict(model, image):
    image = transform(image)
    image = image.unsqueeze(0).to(device)
    with torch.no_grad():
        if torch.cuda.is_available():
            outputs = model.cuda()(image)
        else:
            outputs = model(image)
    return get_point(outputs)


def main():
    model = get_model(keypoints = 7)
    model.eval()
    x0, y0, x1, y1 = select_window()

    using_ai = True
    while True:
        if keyboard.is_pressed('q'):
            using_ai = not using_ai
            if not using_ai:
                print('AI disabled. Press <q> to enable it.')
        if using_ai:
            img = capture_screenshot(y0, x0, x1-x0, y1-y0)
            headlist, bodylist = predict(model, img)
            target_x = 0
            target_y = 0
            if len(headlist)>0:
                target_x = int(x0+headlist[0][0])
                target_y = int(y0+headlist[0][1])
                print("shooting head:", target_x, target_y)
            elif len(bodylist)>0:
                target_x = int(x0+bodylist[0][0])
                target_y = int(y0+bodylist[0][1])
                print("shooting body:", target_x, target_y)
            if target_x!=0 and target_y!=0:
                shoot(target_x, target_y)
            else:
                print("No target detected")
main()
